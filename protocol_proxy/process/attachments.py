from __future__ import annotations
import base64
import copy
from typing import List
from settings import DEBUG
from models.allegato import Allegato
from models.auth_token import AuthClient
from models.types import FlowState, TipoAllegato
from soap.soap_client import SoapClient
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def process_attachment_list(
    protocollo: dict,
    configuration: dict,
    client: SoapClient,
    message: dict,
    flow_message: FlowState,
) -> List[dict]:
    """
    Processa la lista di allegati.
    """
    log.debug("Caricamento allegati")
    token = client.get_token(
        configuration["user_proxy"], configuration["password"], configuration["dominio"]
    )
    try:
        processed_attachment: List[dict] = []
        not_processed_list_attachments: List[dict] = message["attachments"]
        if flow_message == FlowState.PARTIAL_REGISTRATION:
            processed_attachments = message["retry_meta"]["attachments"]
            for attachment in processed_attachments:
                if attachment in not_processed_list_attachments:
                    not_processed_list_attachments.remove(attachment)

        for attachment in not_processed_list_attachments:
            # Estrai i dati dell'allegato
            name = attachment["name"]
            url = attachment["url"]
            # Esegui il processamento dell'allegato
            file_base64 = download_and_encode_pdf(url, message["tenant_id"])
            if file_base64 is None:
                continue
            else:
                log.debug(f"Caricamento allegato {name}")
                client.put_allegato(
                    token,
                    protocollo["annoProtocollo"],
                    protocollo["numeroProtocollo"],
                    protocollo["tipoProtocollo"],
                    TipoAllegato.ALLEGATO.value,
                    name,
                    file_base64,
                )
                # rimuovi attachment from attachments
                processed_attachment.append(attachment)
                log.debug(f"Allegato {name} caricato correttamente")
        # controlla che la lista di allegati sia vuota
        client.destroy_token(token, configuration["dominio"])
        return processed_attachment
    except Exception as e:
        log.error(f"Errore durante il caricamento degli allegati: {e}", exc_info=DEBUG)
        client.destroy_token(token, configuration["dominio"])
        return processed_attachment


def get_attachment(name: str, url: str, tenant_id: str) -> Allegato:
    file_base64 = download_and_encode_pdf(url, tenant_id)
    if file_base64 is None:
        return None
    attachment = Allegato(name, file_base64)
    log.debug(f"Creazione Allegato:{attachment}")
    return attachment


def download_and_encode_pdf(url: str, tenant_id: str) -> str:
    auth_client = AuthClient(tenant_id)
    try:
        # Effettua una richiesta GET per scaricare il contenuto del PDF dalla URL
        log.debug(f"Richiesta GET per scaricare il contenuto del PDF dalla URL: {url}")
        response = auth_client.download_file_with_auth(url)
        # Verifica se la richiesta è stata eseguita con successo (codice 200)
        if response.status_code == 200:
            # Ottieni il contenuto del PDF
            pdf_content = response.content
            # Converti il contenuto del PDF in base64
            pdf_base64 = base64.b64encode(pdf_content).decode("utf-8")
            return pdf_base64
        else:
            # Se la richiesta non è andata a buon fine, stampa un messaggio di errore
            log.error(
                f"Errore nella richiesta. Codice: {response.status_code}",
                exc_info=DEBUG,
            )
            return None
    except Exception as e:
        # Se si verifica un errore durante la richiesta, stampa un messaggio di errore
        log.error(f"Errore durante la richiesta GET: {e}", exc_info=DEBUG)
        return None
