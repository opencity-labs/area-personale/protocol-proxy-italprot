from __future__ import annotations
from typing import List
from models.mittenete_destinatario import MittenteDestinatario
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()


def get_list_author(message: dict) -> List:
    log.debug("Creazione mittenti")
    lista_mittenti = []
    for author in message["author"]:
        mittente = MittenteDestinatario.from_dict(author)
        lista_mittenti.append(mittente)
    log.debug(lista_mittenti)
    return lista_mittenti


def get_configuration_author(conf_services: dict) -> List:
    log.debug("Creazione destinatario")
    lista_destinatari = []
    # crea il destinatario
    destinatario = MittenteDestinatario(
        conf_services["denominazione"],
        email=conf_services["email"],
        codice=conf_services["codice_ufficio"],
    )
    lista_destinatari.append(destinatario)
    log.debug(lista_destinatari)
    return lista_destinatari
