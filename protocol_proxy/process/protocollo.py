from __future__ import annotations
from zeep.helpers import serialize_object
from configuration.utils import get_storage_protocolli
from settings import DEBUG
from models.protocollo import Protocollo
from models.types import TrasmissionType, TipoProtocollo
from process.attachments import get_attachment
from process.authors import get_configuration_author, get_list_author
from models.excepitions import MainDocumentException
from models.mittenete_destinatario import Firmatario
from soap.soap_client import SoapClient
from storage.storage_manager import StorageManager
from models.logger import get_logger

# Ottieni il logger dalla configurazione centralizzata
log = get_logger()

storage_manager = StorageManager()


def process_message_protocollo(
    message: dict, configuration: dict, client: SoapClient
) -> dict:
    log.debug("Get token from client")
    token = client.get_token(
        configuration["user_proxy"], configuration["password"], configuration["dominio"]
    )

    # protocollo il messaggio
    protocollo = create_protocollo(message, configuration)
    log.debug("Send protocol")
    response_protocollo = client.put_protocollo(token, protocollo)
    response_protocollo = dict(serialize_object(response_protocollo))
    client.destroy_token(token, configuration["dominio"])
    if response_protocollo["messageResult"]["tipoRisultato"] == "Error":
        log.error(
            f"Errore durante il salvataggio del protocollo: {response_protocollo['messageResult']['descrizione']}",
            exc_info=DEBUG,
        )
        raise MainDocumentException()
    log.debug(response_protocollo)
    return response_protocollo


def create_protocollo(
    message: dict,
    conf_services: dict,
) -> Protocollo:
    log.debug("Creazione protocollo")
    # trasforma il json in dict
    firmatari = []
    if (
        message["registration_data"]["transmission_type"]
        == TrasmissionType.INBOUND.value
    ):
        tipo_protocollo = TipoProtocollo.ARRIVO.value
        lista_mittenti = get_list_author(message)
        lista_destinatari = get_configuration_author(conf_services)
    else:
        firmatari.append(
            Firmatario(
                conf_services["codice_firmatario"],
                conf_services["codice_ufficio_firmatario"],
            )
        )
        tipo_protocollo = TipoProtocollo.PARTENZA.value
        lista_destinatari = get_list_author(message)
        lista_mittenti = get_configuration_author(conf_services)

    protocollo_stored = get_storage_protocolli(message["remote_id"])

    main_document = get_attachment(
        message["main_document"]["name"],
        message["main_document"]["url"],
        message["tenant_id"],
    )
    protocollo = Protocollo(
        tipo_protocollo,
        conf_services["codice_ufficio"],
        message["description"],
        lista_mittenti,
        lista_destinatari,
        conf_services["classificazione"],
        main_document,
        conf_services["document_type"] if "document_type" in conf_services else "DGEN",
        firmatari,
        numero_protocollo_antecedente=protocollo_stored["numeroProtocollo"],
        anno_protocollo_antecedente=protocollo_stored["annoProtocollo"],
        tipo_protocollo_antecedente=protocollo_stored["tipoProtocollo"],
    )
    log.debug(protocollo)
    return protocollo
