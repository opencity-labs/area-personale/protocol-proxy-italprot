from __future__ import annotations
from models.logger import get_logger
from zeep import Client, Plugin, Transport
from requests import Session
from zeep.exceptions import Fault
from models.protocollo import Protocollo

log = get_logger()


class SoapPlugin(Plugin):
    """
    Plugin di Zeep per memorizzare la richiesta e la risposta SOAP su log
    """

    def ingress(self, envelope, http_headers, operation):
        return envelope, http_headers

    def egress(self, envelope, http_headers, operation, binding_options):
        return envelope, http_headers


class SoapClient(object):
    def __init__(self, wsdl, ws_url: str):
        session = Session()
        session.verify = False

        transport = Transport(
            session=session,
        )

        self._client = Client(wsdl, plugins=[SoapPlugin()], transport=transport)
        self.service = self._client.create_service(
            "{urn:proWsProtocollo}proWsProtocolloBinding",
            ws_url,
        )

    def get_token(self, user: str, pwd: str, domain: str) -> str:
        """
        Metodo GetItaEngineContextToken
        Fornisce un token di sessione per un ente specifico serve per l'accesso ai servizi.
        Il numero di sessioni che si possono rilasciare contemporaneamente e il loro tempo
        di inattività è configurabile per singolo utente.
        Argomenti in Input:
            userName: Nome utente (str)
            userPassword: Password utente (str)
            domainCode: Codice ente (str)
        Dati in Output GetItaEngineContextTokenResponse:
            return Token di sessione (str)
        """
        response = self.service.GetItaEngineContextToken(
            userName=user, userPassword=pwd, domainCode=domain
        )
        log.debug(f"Response call:{response}")
        return response

    def check_token(self, token: str, domain: str | None = "") -> str:
        """
        CheckItaEngineContextToken
        Controlla la validità di un token di sessione.
            Args:
                token (str): oken di sessione
                domainCode (str): domainCode

            Returns:
                response (str): uguale a "Valid" se il token è valido, in caso contrario sarà restituito un oggetto soap-fault
        """
        try:
            response = self.service.CheckItaEngineContextToken(
                token=token, domainCode=domain
            )
            log.debug(f"Response call:{response}")
            return response
        except Fault as e:
            return str(e)

    def destroy_token(self, token: str, domain: str) -> str:
        """
        Metodo DestroyItaEngineContextToken
        Annulla un token di sessione, l'utilizzo di tale metodo è consigliato sia per minimizzare le funzioni
        automatiche di pulizia delle sessioni scadute, che per evitare che il sistema blocchi l'accesso a nuove
        sessioni per il raggiunto limite di sessioni attive contemporaneamente.

        Argomenti in Input:
          token (str): Token di sessione
          domainCode (str): Codice ente
        Dati in Output DestroyItaEngineContextTokenResponse:
          response (str):"Success", Se il token è valido, in caso contrario sarà restituito un oggetto soap-fault
        """
        try:
            response = self.service.DestroyItaEngineContextToken(
                token=token, domainCode=domain
            )
            log.debug(f"Response call:{response}")
            return response
        except Fault as e:
            return str(e)

    def get_protocollo(
        self,
        token: str,
        anno: str,
        numero: str,
        tipo: str,
        segnatura: str | None = None,
    ) -> dict:
        """
        Metodo GetProtocollo
        Si tratta di un servizio che estrae tutte le informazioni relative ad una registrazione di protocollo.
        I parametri anno, numero e tipo vanno compilati se la ricerca è per numero in alternativa è possibile
        utilizzare la segnatura completa. Se nessuno dei tre parametri è specificato il programma restituirà un array
        vuoto.
        Argomenti di input:
            token (str): Token di sessionevalido
            anno (str): Anno del protocollo
            numero (str): Numero del protocollo
            tipo (str): Tipo del protocollo
            segnatura (str|None): Segnatura del protocollo.
        Dati in Output GetProtocolloResponse:
            return:[Struttura]
        """
        response = self.service.GetProtocollo(
            token=token, anno=anno, numero=numero, tipo=tipo, segnatura=segnatura
        )
        log.debug(f"Response call:{response}")
        return response

    def put_protocollo(
        self,
        token: str,
        protocollo: Protocollo,
    ):
        """
        Metodo PutProtocollo
            Si tratta di un servizio in grado di inserire una registrazione di protocollo
            token (str): Token di sessione valido.
            datiProtocollo (Protocollo):
            Gli allegati del protocollo possono essere trasmessi in due modi:
            1) Allegato principale contestuale all’inserimento del protocollo e gli eventuali allegati aggiuntivi aggiunti in un secondo momento tramite il metodo “PutAllegato”
                Allegato Principale contestuale all’inserimento del protocollo:
                    id: Identificativo del file (non usato)
                    tipoFile: Tipo di file (ininfluente sempre PRINCIPALE)
                    nomeFile: Nome del file, compreso di estensione.
                    estensione: Estensione del file (non usato)
                    stream: Stream Base 64 dell'allegato
                    note: Note per l'allegato
                    marcaDocumento: Flag per indicare se il documento pdf allegato è da marcare con la segnatura (boolean)
                    mettiAllaFirma: Flag per indicare se il documento deve essere messo alla firma per il Firmatario (valido per P e C ) (boolean)
            2) Allegati caricati precedentemente, tramite il metodo “InsertDocumento” ed associati al protocollo al momento della protocollazione
                Allegati Caricati precedentemente::
                    idunivoco: Identificativo univoco del file
                    hashfile: Impronta del file caricato (sha256)
                    nomeFile: Nome del file, compreso di estensione.
                    tipoFile: Tipo di file (PRINCIPALE o ALLEGATO)
                    estensione: Estensione del file
                    note: Note per l'allegato
                    marcaDocumento: Flag per indicare se il documento pdf allegato è da marcare con la segnatura (boolean)
                    mettiAllaFirma: Flag per indicare se il documento deve essere messo alla firma per il Firmatario (valido per P e C ) (boolean)

        Dati in Output PutProtocolloResponse:
                    items: [Struttura]
                      rowidProtocollo: Id record protocollo
                      annoProtocollo: Anno del Protocollo
                      numeroProtocollo: Numero del protocollo assegnato
                      tipoProtocollo: Tipo Protocollo (A/P/C)
                      dataProtocollo: Data del protocollo
                      segnatura: Segnatura del protocollo creato
                      messageResult:[Struttura]
                          descrizione: (descrizione dell'esito)
                          tipoRisultato: (Info, Error, Warning)
        """
        response = self.service.PutProtocollo(
            token=token, datiProtocollo=protocollo.to_dict()
        )
        log.debug(f"Response call:{response}")
        return response

    def get_allegato(
        self,
        token: str,
        _id: int,
        block_size: int | None = -1,
        part: int | None = -1,
    ) -> dict:
        """
        Metodo GetAllegato
        Questo metodo consente il recupero di un file allegato al protocollo. Opzionalmente è possibile
        anche definire un blocksize e un part, per recuperare file di grandi dimensioni non in un’unica soluzione,
        ma un blocco alla volta. Sarà compito del client ricostruire i vari blocchi per comporre l’intera stringa base64.
        Ad esempio, se si definisce blockSize = 1024 e part = 1, verrà restituito il primo MB dello stream in base 64.

        Argomenti di input:
            token (str): Token di sessionevalido
            id (int): Identificativodell'allegato
            blockSize (int|None): Se valorizzato indica la dimensione in KB del blocco da trasferire
            part (int|None): Indica il numero del blocco da far ritornare
        Dati in Output GetAllegatoResponse:
            return:[Struttura]
                allegato:[Struttura]
                    id (int): Identificativo dell'allegato
                    tipoFile (str): Nome del file allegato
                    nomeFile (str): Nome del file allegato
                    estensione (str): Estensione del file allegato
                    stream (str): Stream base 64 del file allegato. Nota bene nel caso di download in parti il valore vuoto indica che il download è completato
                    note (str): Note dell'allegato
                    part (int): Numero progressivo parte allegato scaricata
                    marcaDocumento (str): None,
                    mettiAllaFirma (str): None,
                    sha (str): None
                messageResult:[Struttura]
                    descrizione (str): Descrizione dell'esito
                    tipoRisultato (str): Tipo di esito
        """
        response = self.service.GetAllegato(
            token=token, id=_id, blockSize=block_size, part=part
        )
        log.debug(f"Response call:{response}")
        return response

    def put_allegato(
        self,
        token: str,
        anno: str,
        numero: str,
        tipo: str,
        tipoFile: str,
        nomeFile: str,
        stream: str,
        note: str | None = "",
        marcaDocumento: bool | None = 0,
        mettiAllaFirma: bool | None = 0,
    ):
        """
        Metodo PutAllegato
        Questo metodo consente l'inserimento di un file allegato al protocollo.
        Argomenti di input:
            token (str): Token di sessionevalido
            anno (str (4)): Anno del protocollo
            numero: Numero di registrazione del protocollo
            tipo (str): Tipo di protocollo (A, P, C)
            tipoFile (str): Tipologia file (PRINCIPALE o ALLEGATO)
            nomeFile (str): Nome del file con estensione: esempio.pdf
            estensione (str): Estensione del documento. Esempio: pdf,rtf, ecc...
            stream (str): Base 64 del file
            note (str): Descrizione del documento
            marcaDocumento (bool): Flag per indicare se il documento pdf allegato è da marcare con la segnatura
            mettiAllaFirma (bool): Flag per indicare se il documento deve essere messo alla firma per il Firmatario (valido per P e C )
        Dati in Output PutAllegatoResponse:
            messageResult: [Struttura]
                descrizione: (descrizione dell'esito)
                tipoRisultato: (Info, Error, Warning)
        """
        file_extension = nomeFile.rsplit(".", 1)
        estensione = file_extension[-1]
        response = self.service.PutAllegato(
            token=token,
            anno=anno,
            numero=numero,
            tipo=tipo,
            tipoFile=tipoFile,
            nomeFile=nomeFile,
            estensione=estensione,
            stream=stream,
            note=note,
            marcaDocumento=marcaDocumento,
            mettiAllaFirma=mettiAllaFirma,
        )
        log.debug(f"Response call:{response}")
        return response

    def insert_documento(self, token: str, nome_file: str, stream: str):
        """
        Metodo InsertDocumento
        Si tratta di un servizio in grado di precaricare un documento che verrà poi associato ad un protocollo.

        Argomenti di input:
            token (str): Token di sessione valido
            nomeFile (str): Nome del file con estensione: esempio.pdf
            stream (str): Base 64 del file

        Dati in Output InsertDocumentoResponse:
        allegatoPrecaricato: [Struttura]
            idunivoco: ID Univoco del file inserito
            hashfile: Impronta del file inserito
            messageResult: [Struttura]
                descrizione: (descrizione dell'esito)
                tipoRisultato: (Info, Error, Warning)
        """
        response = self.service.InsertDocumento(
            token=token, nomeFile=nome_file, stream=stream
        )
        log.debug(f"Response call:{response}")
        return response

    def get_organigramma(self, token: str) -> dict:
        """
        Metodo GetOrganigramma
        Questo metodo consente la recupero dell'organigramma.

        Argomenti di input:
            token (str): Token di sessione valido

        Dati in Output GetOrganigrammaResponse:
            return:[Struttura]
                descrizioneUfficio: Descrizione Ufficio di appartenenza
                codiceUtente: Codice associato all’utente
                nominativo: Nominativo dell’utente
                ruolo: Descrizione del ruolo
        """
        response = self.service.GetOrganigramma(token=token)
        log.debug(f"Response call:{response}")
        return response

    def mail_notify_protocollo(
        self,
        token: str,
        anno: str,
        numero: str,
        tipo: str,
        subjext: str | None = "",
        context: str | None = "",
    ) -> dict:
        """
        Metodo NotificaMailProtocollo
        Si tratta di un servizio permette di inviare le notifiche ai destinatari di un protocollo

        Argomenti di input:
            Token (str): Token di sessione valido
            anno (str(4)): Anno del protocollo
            numero (str): Numero di registrazione del protocollo
            tipo (str): Tipo di protocollo (A, P, C)

        Dati in Output NotificaMailProtocollo:
        statoNotifica: [Struttura]
            stato (int): Esiti: 0 Errore in invio, 1 Inviato Correttamete, 2 Nessun destinatario a cui inviare la notifica
            messageResult: [Struttura]
                descrizione (str): (descrizione dell'esito)
                tipoRisultato (str): (Info, Error, Warning)
        """
        response = self.service.NotificaMailProtocollo(
            token=token,
            anno=anno,
            numero=numero,
            tipo=tipo,
            oggettoCustom=subjext,
            bodyCustom=context,
        )
        log.debug(f"Response call:{response}")
        return response

    def get_notify_protocollo(
        self, token: str, anno: str, numero: str, tipo: str, rowidmail: str
    ) -> dict:
        """
        Metodo GetNotificaMailProtocollo
        Si tratta di un servizio permette di ricavare il contenuto di una notifica mail relativa al protocollo.

        Argomenti di input:
            Token: Token di sessione valido. (String)
            anno: Anno del protocollo. (String (4))
            numero: Numero di registrazione del protocollo. (String)
            tipo: Tipo di protocollo (A, P, C) (String)
            rowidmail: Rowid della mail da cerecare (String)

        Dati in Output GetNotificaMailProtocollo:
        notificaMail: [Struttura]
            stream: Base64 della notifica ricercata (la notifica è in formato eml)
            messageResult: [Struttura]
                descrizione: (descrizione dell'esito)
                tipoRisultato: (Info, Error, Warning)
        """
        response = self.service.GetNotificaMailProtocollo(
            token=token, anno=anno, numero=numero, tipo=tipo, rowidmail=rowidmail
        )
        log.debug(f"Response call:{response}")
        return response
