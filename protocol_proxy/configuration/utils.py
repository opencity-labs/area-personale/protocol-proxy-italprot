import json
from models.logger import get_logger
from storage.storage_manager import StorageManager
from settings import DEBUG

log = get_logger()
storage_manager = StorageManager()


def read_local_schema():
    try:
        with open("./configuration/schema.json", "r") as file:
            return file.read()
    except Exception as e:
        log.error(f"Error reading schema: {e}", exc_info=DEBUG)
        return None


def save_configuration(dicionary_configuration: dict):
    try:
        file_path = f"{dicionary_configuration['id']}.json"
        if not storage_manager.save(file_path, dicionary_configuration):
            raise Exception("Error saving configuration")
        log.debug(f"Configuration saved in: {file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def check_configuration(key: str) -> dict:
    try:
        conf_services = storage_manager.read(f"{key}.json")
        log.debug(f"Configuration read from: {key}.json")
        return json.loads(conf_services)
    except Exception as e:
        log.warning(f"Error reading configuration: {e}", exc_info=DEBUG)
        return None


def delete_configuration(key: str) -> bool:
    try:
        response = storage_manager.delete(f"{key}.json")
        if response:
            log.debug(f"Configuration deleted from: {key}.json")
        else:
            log.error(f"Configuration not deleted from: {key}.json", exc_info=DEBUG)
        return response
    except Exception as e:
        log.error(f"Error deleting configuration: {e}", exc_info=DEBUG)
        return False


def save_storage_protocolli(dicionary_configuration: dict, remote_id: str):
    try:
        dictionary_to_push = {remote_id: dicionary_configuration}
        file_path = "storage_protocolli.json"
        if not storage_manager.save(file_path, dictionary_to_push):
            raise Exception("Error saving configuration")
        log.debug(f"Configuration saved in: {file_path}")
    except Exception as e:
        log.error(f"Error saving configuration: {e}", exc_info=DEBUG)


def get_storage_protocolli(remote_id_protocollo: str) -> dict:
    """
    Retrieve the protocol data for the given remote_id_protocollo from storage.

    Args:
        remote_id_protocollo (str): The ID of the protocol to retrieve.

    Returns:
        dict: The protocol data, or a blank protocol template if not found.
    """
    blank_dict = {
        "annoProtocollo": None,
        "numeroProtocollo": None,
        "tipoProtocollo": None,
    }

    try:
        # Read and parse the storage file
        protocollo_stored = storage_manager.read("storage_protocolli.json")
        if protocollo_stored is None:
            log.info("Storage is empty, initializing with default template.")
            protocollo_stored = {"storage": blank_dict}
            save_storage_protocolli(protocollo_stored, "blank")
        else:
            protocollo_stored = json.loads(protocollo_stored)

        log.debug("Configuration read from: storage_protocolli.json")

        # Retrieve the protocol data or return the blank dictionary
        return protocollo_stored.get(remote_id_protocollo, blank_dict)

    except json.JSONDecodeError as e:
        log.error(f"Error decoding JSON from storage: {e}", exc_info=DEBUG)
    except Exception as e:
        log.error(f"Unexpected error reading configuration: {e}", exc_info=DEBUG)

    return blank_dict
