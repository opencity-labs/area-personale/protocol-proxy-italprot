from typing import Optional, Union
from fastapi.responses import JSONResponse
from pydantic import BaseModel, Field, field_validator

ERR_422_SCHEMA = {
    "description": "Validation Error",
    "content": {
        "application/problem+json": {
            "schema": {
                "$ref": "#/components/schemas/ErrorMessage",
            },
        },
    },
}


class Status(BaseModel):
    status: str


class ErrorMessage(BaseModel):
    type: str
    title: str
    status: Optional[int] = Field(..., format="int32")
    detail: Union[str, dict]
    instance: Optional[str]

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenantPut(BaseModel):
    aoo_code: str
    base_url: str
    created_at: str
    description: str
    institution_code: str
    modified_at: str
    slug: str

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationTenant(ConfigurationTenantPut):
    id: str


class ConfigurationTenantPatch(ConfigurationTenantPut):
    aoo_code: str = ""
    base_url: str = ""
    created_at: str = ""
    description: str = ""
    institution_code: str = ""
    modified_at: str = ""
    slug: str = ""


class ConfigurationServicePut(BaseModel):
    is_active: bool
    ws_url: str
    user_proxy: str
    password: str
    dominio: str
    codice_firmatario: str
    codice_ufficio_firmatario: str
    denominazione: str
    codice_ufficio: str
    classificazione: str
    document_type: Optional[str] = ""
    email: Optional[str] = ""

    @field_validator("document_type", mode="before")
    def set_default_document_type(cls, v):
        return v or "DGEN"

    def to_dict(self) -> dict:
        return self.dict()


class ConfigurationServicePatch(ConfigurationServicePut):
    is_active: bool = True
    ws_url: str = ""
    user_proxy: str = ""
    password: str = ""
    dominio: str = ""
    codice_firmatario: str = ""
    codice_ufficio_firmatario: str = ""
    denominazione: str = ""
    codice_ufficio: str = ""
    classificazione: str = ""
    document_type: str = ""
    email: Optional[str] = ""


class ConfigurationService(ConfigurationServicePut):
    id: str
    tenant_id: str


class JSONResponseCustom(JSONResponse):
    media_type = "application/problem+json"
