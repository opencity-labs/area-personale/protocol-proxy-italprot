from models.types import TipoAllegato


class Allegato:
    """
    id Identificativo del file (non usato) F
    tipoFile Tipo di file (ininfluente sempre PRINCIPALE) F
    nomeFile Nome del file, compreso di estensione. O
    estensione Estensione del file (non usato)
    stream Stream Base 64 dell'allegato O
    note Note per l'allegato F
    marcaDocumento Flag per indicare se il documento pdf allegato è da

    marcare con la segnatura

    F (boolean)

    mettiAllaFirma Flag per indicare se il documento deve essere messo
    alla firma per il Firmatario (valido per P e C )

    F (boolean)
    """

    def __init__(
        self,
        nome_file: str,
        stream: str,
        note: str | None = "",
        mark: bool | None = 0,
        signature: bool | None = 0,
    ):
        self.tipoFile = TipoAllegato.PRINCIPALE.value
        self.nomeFile = nome_file
        self.stream = stream
        self.note = note
        self.marcaDocumento = mark
        self.mettiAllaFirma = signature
        file_extension = nome_file.rsplit(".", 1)
        self.file_extension = file_extension[-1]

    def to_dict(self):
        allegato_dict = {
            "id": "",
            "tipoFile": self.tipoFile,
            "nomeFile": self.nomeFile,
            "estensione": self.file_extension,
            "stream": self.stream,
            "note": self.note,
            "marcaDocumento": self.marcaDocumento,
            "mettiAllaFirma": self.mettiAllaFirma,
            "sha": "",
        }

        return allegato_dict


class AllegatoPrecaricato:
    """
    allegatoPrecaricato F
    idunivoco Identificativo univoco del file O
    hashfile Impronta del file caricato (sha256) O
    nomeFile Nome del file, compreso di estensione. O
    tipoFile Tipo di file (PRINCIPALE o ALLEGATO) O
    estensione Estensione del file
    note Note per l'allegato F
    marcaDocumento Flag per indicare se il documento pdf allegato è da
    marcare con la segnatura F (boolean)
    mettiAllaFirma Flag per indicare se il documento deve essere messo
    alla firma per il Firmatario (valido per P e C )F (boolean)
    """

    def __init__(
        self,
        id: str,
        hash_file: str,
        nome_file: str,
        tipo_file: TipoAllegato,
        note: str | None = None,
        mark: bool | None = False,
        signature: bool | None = False,
    ):
        self.id = id
        self.hashfile = hash_file
        self.nomeFile = nome_file
        self.tipoFile = tipo_file.value
        self.note = note
        self.marcaDocumento = mark
        self.mettiAllaFirma = signature
        file_extension = nome_file.rsplit(".", 1)
        self.file_extension = file_extension[-1]

    def to_dict(self):
        allegato_dict = {
            "allegatoPrecaricato": {
                "idunivoco": self.id,
                "hashfile": self.hashfile,
                "nomeFile": self.nomeFile,
                "tipo_file": self.tipoFile,
                "estensione": self.file_extension,
                "note": self.note,
                "marcaDocumento": self.marcaDocumento,
                "mettiAllaFirma": self.mettiAllaFirma,
            }
        }

        return allegato_dict
