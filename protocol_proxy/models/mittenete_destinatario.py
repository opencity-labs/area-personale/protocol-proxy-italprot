class MittenteDestinatario:
    """
    codice: Codice del mittente/destinatario
    denominazione: Denominazione del mittente/destinatario
    indirizzo: Indirizzo del mittente/destinatario
    cap: Cap del mittente/destinatario
    citta: Città del mittente/destinatario
    prov: Provincia del mittente/destinatario
    email: Email del mittente/destintario
    ufficio: Ufficio del mittente/destinatario
    codiceFiscale: Codice fiscale del mittente/destinatario
    tipoSoggetto: Tipo di soggetto del mittente/destinatario ( human o ente)
    """

    def __init__(
        self,
        denominazione: str,
        codice: str,
        email: str,
        indirizzo: str | None = None,
        cap: str | None = None,
        citta: str | None = None,
        prov: str | None = None,
        codiceFiscale: str | None = None,
        tipoSoggetto: str | None = None,
        ufficio: str | None = None,
        CC: str | None = None,
        notificheMail: str | None = None,
        codiceIPASoggetto: str | None = None,
    ):
        self.denominazione = denominazione
        self.codice = codice
        self.indirizzo = indirizzo
        self.cap = cap
        self.citta = citta
        self.prov = prov
        self.email = email
        self.codiceFiscale = codiceFiscale
        self.tipoSoggetto = tipoSoggetto
        self.ufficio = ufficio
        self.CC = CC
        self.notificheMail = notificheMail
        self.codiceIPASoggetto = codiceIPASoggetto

    @classmethod
    def from_dict(cls, data_author: dict):
        denominazione = data_author.get("name")
        family_name = data_author.get("family_name")
        if family_name is not None:
            denominazione = denominazione + " " + family_name
        indirizzo = data_author.get("street_name")
        n_civico = data_author.get("building_number")
        if n_civico is not None:
            indirizzo = indirizzo + " " + n_civico
        codiceFiscale = data_author.get("tax_identification_number")
        citta = data_author.get("town_name")
        prov = data_author.get("country_subdivision")
        email = data_author.get("email")
        # Assegna street_name a indirizzo solo se presente
        cap = data_author.get("postal_code")
        tipoSoggetto = data_author.get("type")

        return cls(
            denominazione=denominazione,
            email=email,
            indirizzo=indirizzo,
            cap=cap,
            citta=citta,
            prov=prov,
            codiceFiscale=codiceFiscale,
            tipoSoggetto=tipoSoggetto,
            codice=None,
            ufficio=None,
            CC=None,
            notificheMail=None,
            codiceIPASoggetto=None,
        )

    def to_dict(self) -> dict:
        mittente_dict = {
            "mittenteDestinatario": {
                "codice": self.codice,
                "denominazione": self.denominazione,
                "indirizzo": self.indirizzo,
                "cap": self.cap,
                "citta": self.citta,
                "prov": self.prov,
                "email": self.email,
                "codiceFiscale": self.codiceFiscale,
                "tipoSoggetto": self.tipoSoggetto,
                "ufficio": None,
                "CC": None,
                "notificheMail": None,
                "tipoSoggetto": None,
                "codiceIPASoggetto": None,
            }
        }
        return mittente_dict


class Firmatario:
    def __init__(self, codice: str, ufficio: str):
        self.codice = codice
        self.ufficio = ufficio

    def to_dict(self) -> dict:
        firmatario_dict = {
            "firmatario": {"codice": self.codice, "ufficio": self.ufficio}
        }
        return firmatario_dict
