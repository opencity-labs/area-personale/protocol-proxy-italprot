from enum import Enum


class FlowState(Enum):
    DOCUMENT_CREATED = "DOCUMENT_CREATED"
    REGISTRATION_PENDING = "REGISTRATION_PENDING"
    REGISTRATION_COMPLETE = "REGISTRATION_COMPLETE"
    PARTIAL_REGISTRATION = "PARTIAL_REGISTRATION"
    REGISTRATION_FAILED = "REGISTRATION_FAILED"


class RestHeaderClient(Enum):
    X_WWW_FORM_URLENCODED = {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
    }
    MULTIPART_FORM_DATA = {"Content-Type": "multipart/form-data"}
    APPLICATION_JSON = {"Content-Type": "application/json"}


class MethodApi(Enum):
    GET = "GET"
    POST = "POST"
    PUT = "PUT"
    DELETE = "DELETE"
    PATCH = "PATCH"
    TRACE = "TRACE"


class TrasmissionType(Enum):
    """
    Enum Description:
    """

    INBOUND = "Inbound"
    OUTOUND = "Outbound"


class TipoProtocollo(Enum):
    ARRIVO = "A"
    PARTENZA = "P"
    COLLEGATO = "C"


class TipoAllegato(Enum):
    ALLEGATO = "ALLEGATO"
    PRINCIPALE = "PRINCIPALE"


class StorageType(Enum):
    AZURE = "azure"
    LOCAL = "local"
    S3 = "s3"
