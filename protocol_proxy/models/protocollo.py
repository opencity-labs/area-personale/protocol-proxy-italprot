from __future__ import annotations
from typing import List
from models.allegato import Allegato, AllegatoPrecaricato
from models.mittenete_destinatario import Firmatario, MittenteDestinatario
from models.types import TipoProtocollo


class Protocollo:
    """
    [Struttura]
    tipoProtocollo: Tipo del Protocollo (A, P, C)
    ufficioOperatore: Codice Ufficio Operatore Inserimento
    firmatari: Firmatari del protocollo, almeno uno se il protocollo è P o C, altrimenti è facoltavio. Il primo indicato è il firmatario principal
        firmatario: Istanza firmatario
            codice Codice Soggetto Firmatario
            ufficio Codice Ufficio Firmatario
    oggetto: Oggetto del Protocollo
    tipoDocumento: Codice Tipo Documento
    numeroProtocolloAntecedente: Numero del protocollo antecedente collegato
    annoProtocolloAntecedente: Anno Protocollo Antecedente
    tipoProtocolloAntecedente: Tipo Protocollo antecedente
    numeroProtocolloMittente: Numero del protocollo mittente
    dataProtocolloMittente: Data del protocollo mittente
    dataArrivo: Data di arrivo del protocollo
    protocolloEmergenza:
    Mittenti: Elenco  mittenteDestinatario Almeno 1 occorrenza, per dove il primo indica il mittente principale. I seguenti sono i MittentiAggiuntivi. [Struttura]
    Destinatari: Elenco  mittenteDestinatario Almeno 1 occorrenza, per la Partenza, dove il primo indica il destinatario principale. I seguenti sono i gli Altri Destinatari. [Struttura]
    """

    def __init__(
        self,
        tipo_protocollo: TipoProtocollo,
        ufficio_operatore: str,
        oggetto: str,
        mittenti: List[MittenteDestinatario],
        destinatari: List[MittenteDestinatario],
        classificazione: str,
        allegato: Allegato | AllegatoPrecaricato,
        tipo_documento: str,
        firmatari: List[Firmatario] | None = [],
        numero_protocollo_antecedente: str | None = None,
        anno_protocollo_antecedente: str | None = None,
        tipo_protocollo_antecedente: str | None = None,
        numero_protocollo_mittente: str | None = None,
        data_protocollo_mittente: str | None = None,
        data_arrivo: str | None = None,
        protocollo_emergenza: str | None = None,
    ):
        self.tipoProtocollo = tipo_protocollo
        self.ufficioOperatore = ufficio_operatore
        if (
            tipo_protocollo in [TipoProtocollo.PARTENZA, TipoProtocollo.COLLEGATO]
            and not firmatari
        ):
            raise Exception("Firmatari obbligatori")
        self.firmatari = firmatari
        self.oggetto = oggetto
        self.tipoDocumento = tipo_documento
        self.numeroProtocolloAntecedente = numero_protocollo_antecedente
        self.annoProtocolloAntecedente = anno_protocollo_antecedente
        self.tipoProtocolloAntecedente = tipo_protocollo_antecedente
        self.numeroProtocolloMittente = numero_protocollo_mittente
        self.dataProtocolloMittente = data_protocollo_mittente
        self.dataArrivo = data_arrivo
        self.protocolloEmergenza = protocollo_emergenza
        self.mittenti = mittenti
        self.destinatari = destinatari
        self.classificazione = classificazione
        self.allegato = allegato
        self.trasmissioni_interne = [Trasmissione(ufficio_operatore)]

    def to_dict(self) -> dict:
        """
        Converte l'istanza di Protocollo in formato JSON.
        """
        protocollo_dict = {
            "tipoProtocollo": str(self.tipoProtocollo),
            "ufficioOperatore": self.ufficioOperatore,
            "firmatari": [firmatario.to_dict() for firmatario in self.firmatari],
            "oggetto": self.oggetto,
            "tipoDocumento": str(self.tipoDocumento) if self.tipoDocumento else None,
            "numeroProtocolloAntecedente": self.numeroProtocolloAntecedente,
            "annoProtocolloAntecedente": self.annoProtocolloAntecedente,
            "tipoProtocolloAntecedente": self.tipoProtocolloAntecedente,
            "numeroProtocolloMittente": self.numeroProtocolloMittente,
            "dataProtocolloMittente": self.dataProtocolloMittente,
            "dataArrivo": self.dataArrivo,
            "protocolloEmergenza": self.protocolloEmergenza,
            "mittenti": [mittente.to_dict() for mittente in self.mittenti],
            "destinatari": [destinatari.to_dict() for destinatari in self.destinatari],
            "trasmissioniInterne": [
                trasmissione.to_dict() for trasmissione in self.trasmissioni_interne
            ],
            "classificazione": self.classificazione,
            "allegato": self.allegato.to_dict(),
        }
        return protocollo_dict


class Trasmissione:
    """
    codiceDestinatario Codice Soggetto destinatario della trasmisione
    descrizioneDestinatario Descrizione destinatario
    codiceUfficio Codice Ufficio del soggetto destinatario
    descrizioneUfficio Descrizione Ufficio
    oggettoTrasmissione Oggetto della Trasmissione (non usato)
    gestione Flag gestione F (boolean)
    responsabile Flag responsabile F (boolean)
    """

    def __init__(
        self,
        codiceUfficio: str,
        codiceDestinatario: str | None = None,
        descrizioneDestinatario: str | None = None,
        descrizioneUfficio: str | None = None,
        oggettoTrasmissione: str | None = None,
        gestione: bool | None = False,
        responsabile: bool | None = False,
    ):
        self.codiceDestinatario = codiceDestinatario
        self.codiceUfficio = codiceUfficio
        descrizioneDestinatario = descrizioneDestinatario
        descrizioneUfficio = descrizioneUfficio
        oggettoTrasmissione = oggettoTrasmissione
        gestione = gestione
        responsabile = responsabile

    def to_dict(self) -> dict:
        trasmissione_dict = {
            "trasmissione": {
                "gestione":True,
                "codiceUfficio": self.codiceUfficio,
                "codiceDestinatario": "",
            }
        }
        return trasmissione_dict
