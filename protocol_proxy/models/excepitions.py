class RetryException(Exception):
    pass


class ProtocolloException(Exception):
    pass


class MainDocumentException(Exception):
    pass


class JsonException(Exception):
    pass


class ExistException(Exception):
    pass


class NotExistingException(Exception):
    pass
